#ifndef SENSORS_H
#define SENSORS_H

#include "base.h"
#include <NewPing.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// lower values allow faster measurements, device maximum is 500 cm
#ifndef MAX_DISTANCE
#define MAX_DISTANCE 200
#endif // MAX_DISTANCE

#ifndef DISTANCE_THRESHOLD
#define DISTANCE_THRESHOLD 20
#endif // DISTANCE_THRESHOLD

// lowest measurement with my sensor: 842, highest: 1007
#ifndef LIGHT_THRESHOLD
#define LIGHT_THRESHOLD 40
#endif // LIGHT_THRESHOLD
  
// change if buttons are bouncy or unresponsive
#ifndef DEBOUNCE_DELAY
#define DEBOUNCE_DELAY 50
#endif // DEBOUNCE_DELAY

// define pins, for faster rearangement when neccessary

// digital pins
// 0 and 1 are not available when using serial communication
// 2-5 are actuator pins
const byte SPRAY_BUTTON  = 6;
const byte MENU_BUTTON   = 7;
const byte NEXT_BUTTON   = 8;
// 9-11 are actuator pins
const byte MAGNET_BUTTON = 12;
// 13 is an actuator pin

// analog pins
// A0 and A1 are actuator pins
const byte DISTANCE_SENSOR    = A2;
const byte TEMPERATURE_SENSOR = A3;
const byte MOTION_SENSOR      = A4;
const byte LDR                = A5;

// .h's
namespace Sensors {
  void init();

  namespace Button {
    struct Button {
      byte pin;
      unsigned long timer = 0;
      bool last_state = false;
      int ctr = 0;
      int total = 0;
    } spray, menu, next, magnet;
          
    void init(Button* button, byte pin);
    
    bool pressed(Button* button);
    void execute_check(Button* button, Promise* self);

    // helper methods because we can't curry
    Promise* wait_for_spray();
    Promise* wait_for_menu();
    Promise* wait_for_next();
    Promise* wait_for_magnet();
  }    


  namespace Motion {
    Promise* promise;

    void init();

    void execute_check(Promise* self);
    Promise* wait_for();
  }

  namespace Distance {
    
    // distances in cm
    int last_measurement;

    // wait at least this time between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
    const int delay_measurement = 50;

    unsigned long timer;

    Promise* promise;

    // using single pin mode
    NewPing sonar(DISTANCE_SENSOR, DISTANCE_SENSOR, MAX_DISTANCE);

    void execute_check(Promise* self);
    Promise* wait_for();
  }

  namespace Light {
    // fill promise when change in distance is bigger then threshold
    int last_measurement;
    int last_delta;
    int delta;
  
    void init();
    void tick();
    
    void execute_check_on(Promise* self);
    void execute_check_off(Promise* self);
    Promise* wait_for_on();
    Promise* wait_for_off();
  }
  
  namespace Temperature {
    OneWire oneWire(TEMPERATURE_SENSOR);
    DallasTemperature sensors(&oneWire);

    void init();
        
    void execute_check(Promise* self);
    Promise* wait_for();
  }
}

// .cpp's
void Sensors::init() {
  Button::init(&Button::spray, SPRAY_BUTTON);
  Button::init(&Button::menu, MENU_BUTTON);
  Button::init(&Button::next, NEXT_BUTTON);
  Button::init(&Button::magnet, MAGNET_BUTTON);

  Motion::init();
  Light::init();
  Temperature::init();
}

// Buttons
void Sensors::Button::init(Button* button, byte pin) {  
  button->pin = pin;
  pinMode(button->pin, INPUT_PULLUP);
}

bool Sensors::Button::pressed(Button* button) {
  return button->last_state;
}

void Sensors::Button::execute_check(Button* button, Promise* self) {
  bool pressed = digitalRead(button->pin) == LOW;

  if (button->timer == 0) {
    if (pressed != button->last_state) {
      button->timer = Timer::start();
    }
  } else { // timer != 0
    if (button->total > 0 && Timer::elapsed(button->timer) >= DEBOUNCE_DELAY) {
      if (button->ctr * 5 >= button->total * 4) { // >80% of samples high
        Promises::complete(self, (uint32_t)true);
        button->last_state = true;
      } else {
        button->last_state = false;
      }
      button->timer = 0;
      button->ctr = 0;
      button->total = 0;
    } else { // elapsed < DEBOUNCE_DELAY
      if (pressed)
        button->ctr++;
      button->total++;
    }
  }/**/
}

void execute_check_spray(Promise* self){
  execute_check(&Sensors::Button::spray, self);
#ifdef DEBUG
  if (Promises::has_completed(self)) {
    Serial.println(F("[DEBUG] Spray button pressed"));
  }
#endif // DEBUG
}
void execute_check_menu(Promise* self){
  execute_check(&Sensors::Button::menu, self);
#ifdef DEBUG
  if (Promises::has_completed(self)) {
    Serial.println(F("[DEBUG] Menu button pressed"));
  }
#endif // DEBUG
}
void execute_check_next(Promise* self){
  execute_check(&Sensors::Button::next, self);
#ifdef DEBUG
  if (Promises::has_completed(self)) {
    Serial.println(F("[DEBUG] Next button pressed"));
  }
#endif // DEBUG
}
void execute_check_magnet(Promise* self){
  execute_check(&Sensors::Button::magnet, self);
#ifdef DEBUG
  if (Promises::has_completed(self)) {
    Serial.println(F("[DEBUG] Magnet response"));
  }
#endif // DEBUG
}

Promise* Sensors::Button::wait_for_spray(){
  return Promises::create(execute_check_spray);
}
Promise* Sensors::Button::wait_for_menu(){
  return Promises::create(execute_check_menu);
}
Promise* Sensors::Button::wait_for_next(){
  return Promises::create(execute_check_next);
}
Promise* Sensors::Button::wait_for_magnet(){
  return Promises::create(execute_check_magnet);
}

// Motion

void Sensors::Motion::init(){
  pinMode(MOTION_SENSOR, INPUT);
}

void Sensors::Motion::execute_check(Promise* self){
  if (digitalRead(MOTION_SENSOR) == HIGH){
      Promises::complete(self, (uint32_t)true);
  }
}

Promise* Sensors::Motion::wait_for(){
  return Promises::create(execute_check);
}

// Distance
void Sensors::Distance::execute_check(Promise* self){
  if (Timer::elapsed(timer) > delay_measurement){
    int v = sonar.ping_cm();
    timer = Timer::start();

    if (abs(last_measurement - v) > DISTANCE_THRESHOLD){
       Promises::complete(self, (uint32_t)true);
    }

    last_measurement = v;
  }
}

Promise* Sensors::Distance::wait_for(){
  return Promises::create(execute_check);
}

// Light
void Sensors::Light::init() {
  pinMode(LDR, INPUT);
  last_measurement = analogRead(LDR);
}

void Sensors::Light::tick() {
  int v = analogRead(LDR);
  last_delta = delta;
  delta = v - last_measurement + delta / 4 * 3; // revert automagically to 0 when not reaching threshold
  last_measurement = v;
#ifdef DEBUG
  if (abs(delta) > 20) {
    Serial.print(F("[DEBUG] Light delta: "));
    Serial.println(delta);
  }
#endif // DEBUG
}

void Sensors::Light::execute_check_on(Promise* self) {
  Sensors::Light::tick();
  if (delta > LIGHT_THRESHOLD && delta < last_delta) { // On max delta
    Promises::complete(self, (uint32_t)last_delta);
    delta = 0;
    last_delta = 0;
#ifdef DEBUG
    Serial.println(F("[DEBUG] Light intensity risen"));
#endif // DEBUG
  }
}

void Sensors::Light::execute_check_off(Promise* self) {
  Sensors::Light::tick();
  if (delta < -LIGHT_THRESHOLD && delta > last_delta) { // On min delta
    Promises::complete(self, (uint32_t)last_delta);
    delta = 0;
    last_delta = 0;
#ifdef DEBUG
    Serial.println(F("[DEBUG] Light intensity dropped"));
#endif // DEBUG
  }
}

Promise* Sensors::Light::wait_for_on(){
  return Promises::create(execute_check_on);
}

Promise* Sensors::Light::wait_for_off(){
  return Promises::create(execute_check_off);
}

// Temperature
void Sensors::Temperature::init(){
  sensors.begin();
}

void Sensors::Temperature::execute_check(Promise* self){
  if (sensors.isConversionComplete()){
    Promises::complete(self, (uint32_t)sensors.getTempCByIndex(0));
  }
}

Promise* Sensors::Temperature::wait_for(){
  sensors.setWaitForConversion(false);  // makes it async
  sensors.requestTemperaturesByIndex(0);
  sensors.setWaitForConversion(true);
  return Promises::create(execute_check);
}

#endif // SENSORS_H
