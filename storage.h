#ifndef STORAGE_H
#define STORAGE_H

#include <EEPROM.h>

#ifndef STORAGE_ADDRESS
#define STORAGE_ADDRESS 0
#endif // STORAGE_ADDRESS

#ifndef LEARNING_RATE
#define LEARNING_RATE 0.1f
#endif // LEARNING_RATE

const byte c_marker = 0xAA;
const int c_sprays_full = 2400;

// .h's
namespace Storage {
  typedef struct Avg {
    float mean;
    float sd;
  } Avg;
  
  typedef struct UsageMode {
    Avg time;
    Avg motion;
  } UsageMode;
  
  typedef struct StorageData {
    UsageMode number1;
    UsageMode number2;
    UsageMode cleanup;
    float     confidence_threshold;
    int       sprays_remaining;
    byte      spray_warmup_time;
  } StorageData;

  StorageData cache;
  long last_update;
  
  void init();
  void tick();

  void reset_sprays();
  void reset_defaults();
  void serial_export(); 
  void write();
  
  void update_number1(long time, long motion);
  void update_number2(long time, long motion);
  void update_cleanup(long time, long motion);
  
  Avg get_number1_time();
  Avg get_number1_motion();
  
  Avg get_number2_time();
  Avg get_number2_motion();

  Avg get_cleanup_time();
  Avg get_cleanup_motion();

  float get_confidence_threshold();
  void  set_confidence_threshold(float threshold);

  int  get_sprays_remaining();
  void set_sprays_remaining(int sprays);

  byte get_spray_warmup_time();
  void set_spray_warmup_time(byte secs);
}

// .cpp's
void Storage::init() {
  byte marker = EEPROM.read(STORAGE_ADDRESS);
  if (marker == c_marker) {
    EEPROM.get(STORAGE_ADDRESS + 1, cache);
  } else {
    Storage::reset_defaults();
  }
  last_update = Timer::start();
}

void Storage::tick() {
  const long five_hours = 60L * 60L * 5L;
  if (Timer::elapsed_seconds(last_update) >= five_hours) {
    Storage::write();
  }
}

void Storage::reset_sprays() {
  cache = StorageData {
    cache.number1,
    cache.number2,
    cache.cleanup,
    cache.confidence_threshold,
    c_sprays_full,
    cache.spray_warmup_time
  };
}

void Storage::reset_defaults() {
  cache = StorageData { // TODO: set values
    { // number1
      { // time
        70, // mean
        20  // sd
      },
      { // motion
        10000, // mean
        2500   // sd
      }
    },
    
    { // number2
      { // time
        450, // mean
        100  // sd
      },
      { // motion
        100000, // mean
        10000   // sd
      }
    },

    { // cleanup
      { // time
        450, // mean
        100  // sd
      },
      { // motion
        200000, // mean
        50000   // sd
      }
    },
    0.5f ,         // confidence_threshold
    c_sprays_full, // sprays_remaining
    15             // spray_warmup_time
  };
  EEPROM.update(STORAGE_ADDRESS, (byte)0);
}

void Storage::serial_export() {
#ifdef DEBUG
  Serial.print(F("StorageData: "));            Serial.println();
  Serial.print(F("  number1: "));              Serial.println();
  Serial.print(F("    time: "));               Serial.println();
  Serial.print(F("      mean: "));             Serial.println(cache.number1.time.mean);
  Serial.print(F("      sd: "));               Serial.println(cache.number1.time.sd);
  Serial.print(F("    motion: "));             Serial.println();
  Serial.print(F("      mean: "));             Serial.println(cache.number1.motion.mean);
  Serial.print(F("      sd: "));               Serial.println(cache.number1.motion.mean);
  Serial.print(F("  number2: "));              Serial.println();
  Serial.print(F("    time: "));               Serial.println();
  Serial.print(F("      mean: "));             Serial.println(cache.number2.time.mean);
  Serial.print(F("      sd: "));               Serial.println(cache.number2.time.sd);
  Serial.print(F("    motion: "));             Serial.println();
  Serial.print(F("      mean: "));             Serial.println(cache.number2.motion.mean);
  Serial.print(F("      sd: "));               Serial.println(cache.number2.motion.mean);
  Serial.print(F("  cleanup: "));              Serial.println();
  Serial.print(F("    time: "));               Serial.println();
  Serial.print(F("      mean: "));             Serial.println(cache.cleanup.time.mean);
  Serial.print(F("      sd: "));               Serial.println(cache.cleanup.time.sd);
  Serial.print(F("    motion: "));             Serial.println();
  Serial.print(F("      mean: "));             Serial.println(cache.cleanup.motion.mean);
  Serial.print(F("      sd: "));               Serial.println(cache.cleanup.motion.mean);
  Serial.print(F("  confidence_threshold: ")); Serial.println(cache.confidence_threshold);
  Serial.print(F("  sprays_remaining: "));     Serial.println(cache.sprays_remaining);
  Serial.print(F("  spray_warmup_time: "));    Serial.println(cache.spray_warmup_time);
#endif // DEBUG
}

void Storage::write() {
  EEPROM.put(STORAGE_ADDRESS + 1, cache);
  EEPROM.update(STORAGE_ADDRESS, c_marker);
#ifdef DEBUG
  Serial.print(F("[DEBUG] Cache written to EEPROM"));
#endif // DEBUG
}

Storage::Avg update(Storage::Avg old, long value) {
  float new_value = float(value);
  return {
    (1.0f - LEARNING_RATE) * old.mean + LEARNING_RATE * new_value,
    (1.0f - LEARNING_RATE) * old.sd   + LEARNING_RATE * abs(new_value - old.mean)
  };
}

void Storage::update_number1(long time, long motion) {
  cache = StorageData {
    {
      update(cache.number1.time,   time),
      update(cache.number1.motion, motion)
    },
    cache.number2,
    cache.cleanup,
    cache.confidence_threshold,
    cache.sprays_remaining,
    cache.spray_warmup_time
  };
}

void Storage::update_number2(long time, long motion) {
  cache = StorageData {
    cache.number1,
    {
      update(cache.number2.time,   time),
      update(cache.number2.motion, motion)
    },
    cache.cleanup,
    cache.confidence_threshold,
    cache.sprays_remaining,
    cache.spray_warmup_time
  };
}

void Storage::update_cleanup(long time, long motion) {
  cache = StorageData {
    cache.number1,
    cache.number2,
    {
      update(cache.cleanup.time,   time),
      update(cache.cleanup.motion, motion)
    },
    cache.confidence_threshold,
    cache.sprays_remaining,
    cache.spray_warmup_time
  };
}

Storage::Avg Storage::get_number1_time() {
  return cache.number1.time;
}

Storage::Avg Storage::get_number1_motion() {
  return cache.number1.motion;
}

Storage::Avg Storage::get_number2_time() {
  return cache.number2.time;
}

Storage::Avg Storage::get_number2_motion() {
  return cache.number2.motion;
}

Storage::Avg Storage::get_cleanup_time() {
  return cache.cleanup.time;
}

Storage::Avg Storage::get_cleanup_motion() {
  return cache.cleanup.motion;
}

float Storage::get_confidence_threshold() {
  return cache.confidence_threshold;
}

void Storage::set_confidence_threshold(float threshold) {
  cache = StorageData {
    cache.number1,
    cache.number2,
    cache.cleanup,
    threshold,
    cache.sprays_remaining,
    cache.spray_warmup_time
  };
}

int Storage::get_sprays_remaining() {
  return cache.sprays_remaining;
}

void Storage::set_sprays_remaining(int sprays) {
  cache = StorageData {
    cache.number1,
    cache.number2,
    cache.cleanup,
    cache.confidence_threshold,
    sprays,
    cache.spray_warmup_time
  };
}

byte Storage::get_spray_warmup_time() {
  return cache.spray_warmup_time;
}

void Storage::set_spray_warmup_time(byte secs) {
  cache = StorageData {
    cache.number1,
    cache.number2,
    cache.cleanup,
    cache.confidence_threshold,
    cache.sprays_remaining,
    secs
  };
}

#endif // STORAGE_H
