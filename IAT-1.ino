#include "base.h"
#include "storage.h"
#include "freshener.h"
#include "measuring.h"
#include "gui.h"
#include "sensors.h"
#include "actuators.h"

State current     = Measuring::InitializationState;
State current_gui = GUI::IdleState;

Promise* force_spray = 0;
long timer;

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
#endif // DEBUG
  
  Storage::init();
  Actuators::init();
  Sensors::init();

  force_spray = Sensors::Button::wait_for_spray();
  
  StateMgr::initMgr(current);
  StateMgr::initMgr(current_gui);

  timer = Timer::start();
}

void loop() {
  Promises::exec_checks();
  
  Storage::tick();
  Actuators::tick();
  Freshener::tick();

  if (Promises::has_completed(force_spray)) {
    Freshener::exec_spray();
    Promises::finalize(force_spray);
    force_spray = Sensors::Button::wait_for_spray();
  }
  
  StateMgr::tick(&current);
  StateMgr::tick(&current_gui);

  if (Timer::elapsed_seconds(timer) >= 1) {
    Actuators::MultiColorLED::set_color(255, 255, 0);
    timer = Timer::start();
  } else if (Sensors::Button::pressed(&Sensors::Button::menu) ||
             Sensors::Button::pressed(&Sensors::Button::next) ||
             Sensors::Button::pressed(&Sensors::Button::spray)) {
    Actuators::MultiColorLED::set_color(255, 255, 255);
  } else if (Timer::elapsed(timer) >= 500 && (Freshener::queue > 0 || !Actuators::Freshener::available())) {
    Actuators::MultiColorLED::set_color(0, 255, 0);
  } else if (Measuring::debug == Measuring::MEASURING) {
    Actuators::MultiColorLED::set_color(0, 0, 255);
  } else {
    Actuators::MultiColorLED::set_color(255, 0, 0);
  }
}
