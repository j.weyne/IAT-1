#ifndef FRESHENER_H
#define FRESHENER_H

#include "base.h"
#include "actuators.h"

#ifndef SPRAY_INTERVAL
#define SPRAY_INTERVAL 300
#endif //SPRAY_INTERVAL

// .h's
namespace Freshener {
  void action1();
  void action2();

  void tick();
  void exec_spray();
  byte queue = 0;

  // wait for warmup_time to pass
  unsigned long timer;
}

// .cpp's
void Freshener::tick(){
  if (queue > 0 && Actuators::Freshener::available() && Timer::elapsed_seconds(timer) > Storage::get_spray_warmup_time() ){
    Actuators::Freshener::execute_spray();
    queue--;
  }
}

void Freshener::exec_spray() {
  if (Actuators::Freshener::available()) {
    Actuators::Freshener::execute_spray();
  }
}

void Freshener::action1() {
  if (Actuators::Freshener::elapsed_since_last_spray() > SPRAY_INTERVAL){
    queue = 1;
    timer = Timer::start();
  }
}

void Freshener::action2() {
  if (Actuators::Freshener::elapsed_since_last_spray() > SPRAY_INTERVAL){
    queue = 2;
    timer = Timer::start();
  }
}

#endif // SYSTEM_H
