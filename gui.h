#ifndef GUI_H
#define GUI_H

#include "base.h"
#include "sensors.h"
#include "actuators.h"
#include "freshener.h"
#include "measuring.h"
#include "storage.h"

// .h's
namespace GUI {
  namespace {
    // Globals
    Promise* button_l = 0;
    Promise* button_r = 0;
    Promise* temp     = 0;

    long timer;
    byte select;
    byte select_value;
  }

  void IdleState_constr();
  void IdleState_destr();
  void IdleState_tick(State* state);

  void FeedbackState_constr();
  void FeedbackState_destr();
  void FeedbackState_tick(State* state);

  void ResponseState_constr();
  void ResponseState_destr();
  void ResponseState_tick(State* state);

  void MeasuringState_constr();
  void MeasuringState_destr();
  void MeasuringState_tick(State* state);

  namespace Menu {
    typedef enum OverviewMenu {
      RETURN,
      SPRAY_DELAY,
      RESET_SPRAYS,
      ADVANCED
    } OverviewMenu;

    const byte OverviewMenu_length = 4;

    typedef enum AdvancedMenu {
      RETURN_ADV,
      PROMISES,
      LIGHTSENSOR,
      WRITECACHE,
#ifdef DEBUG
      CACHETOSERIAL,
#endif // DEBUG
      RESET_DEFAULTS
    } AdvancedMenu;

#ifdef DEBUG
    const byte AdvancedMenu_length = 6;
#else
    const byte AdvancedMenu_length = 5;
#endif

    void OverviewState_constr();
    void OverviewState_destr();
    void OverviewState_tick(State* state);

    void ResetSprayState_constr();
    void ResetSprayState_destr();
    void ResetSprayState_tick(State* state);
    
    void SetDelayState_constr();
    void SetDelayState_destr();
    void SetDelayState_tick(State* state);
  
    void AdvancedState_constr();
    void AdvancedState_destr();
    void AdvancedState_tick(State* state);
  
    State OverviewState {
      OverviewState_constr,
      OverviewState_destr,
      OverviewState_tick
    };

    State ResetSprayState {
      ResetSprayState_constr,
      ResetSprayState_destr,
      ResetSprayState_tick
    };

    State SetDelayState {
      SetDelayState_constr,
      SetDelayState_destr,
      SetDelayState_tick
    };

    State AdvancedState {
      AdvancedState_constr,
      AdvancedState_destr,
      AdvancedState_tick
    };
  }

  State IdleState {
    IdleState_constr,
    IdleState_destr,
    IdleState_tick
  };

  State FeedbackState {
    FeedbackState_constr,
    FeedbackState_destr,
    FeedbackState_tick
  };
  
  State ResponseState {
    ResponseState_constr,
    ResponseState_destr,
    ResponseState_tick
  };

  State MeasuringState {
    MeasuringState_constr,
    MeasuringState_destr,
    MeasuringState_tick
  };
}

// .cpp's
void DisplayTemp(Promise* temp, byte row = 1) {
  if (Promises::has_completed(temp)) {
    Actuators::LCD::printAt(F("Temp: "), row);
    Actuators::LCD::print((float)temp->result);
    Actuators::LCD::print(F(" C.       "));
    Promises::finalize(temp);
    temp = Sensors::Temperature::wait_for();
  }
}

void GUI::IdleState_constr() {
  Actuators::LCD::printAt(F("Asleep...       "), 0);
  Actuators::LCD::printAt(F("Temp: working..."), 1);
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
  temp     = Sensors::Temperature::wait_for();
}

void GUI::IdleState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
}

void GUI::IdleState_tick(State* state) {
  DisplayTemp(temp);

  if (Measuring::debug == Measuring::MEASURING) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::IdleState to GUI::FeedbackState"));
#endif // DEBUG
    StateMgr::switchTo(state, FeedbackState);
  }
  
  if (Promises::has_completed(button_l) || Promises::has_completed(button_r)) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::IdleState to GUI::Overview"));
#endif // DEBUG
    StateMgr::switchTo(state, Menu::OverviewState);
  }
}

void GUI::FeedbackState_constr() {
  Actuators::LCD::printAt(F(" Give feedback! "), 0);
  Actuators::LCD::printAt(F(" #1   none   #2 "), 1);
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
  timer = 0;
}

void GUI::FeedbackState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
  timer = 0;
}

void GUI::FeedbackState_tick(State* state) {
  if (Measuring::debug != Measuring::MEASURING) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::FeedbackState to GUI::IdleState"));
#endif // DEBUG
    StateMgr::switchTo(state, IdleState);
  }
  
  if (Promises::has_completed(button_l) || Promises::has_completed(button_r)) {
    if (timer == 0) {
      timer = Timer::start();
    } else if (Timer::elapsed(timer) > 200) {
      if (Promises::has_completed(button_l) && Promises::has_completed(button_r)) {
#ifdef DEBUG
        Serial.println(F("[DEBUG] Session type CLEANUP"));
#endif // DEBUG
        Measuring::session = { true, Measuring::CLEANUP };
      } else if (Promises::has_completed(button_l)) {
#ifdef DEBUG
        Serial.println(F("[DEBUG] Session type NUMBER1"));
#endif // DEBUG
        Measuring::session = { true, Measuring::NUMBER1 };
      } else { // button_r
#ifdef DEBUG
        Serial.println(F("[DEBUG] Session type NUMBER2"));
#endif // DEBUG
        Measuring::session = { true, Measuring::NUMBER2 };
      }
#ifdef DEBUG
      Serial.println(F("[DEBUG] Switch from GUI::FeedbackState to GUI::ResponseState"));
#endif // DEBUG
      StateMgr::switchTo(state, ResponseState);
    }
  }
}

void GUI::ResponseState_constr() {
  Actuators::LCD::printAt(F("   Thank you!   "), 0);
  Actuators::LCD::printAt(F("Temp: working..."), 1);
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
  timer = Timer::start();
}

void GUI::ResponseState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
  timer = 0;
}

void GUI::ResponseState_tick(State* state) {
  DisplayTemp(temp);

  if (Timer::elapsed_seconds(timer) > 5) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::ResponseState to GUI::MeasuringState"));
#endif // DEBUG
    StateMgr::switchTo(state, MeasuringState);
  }

  if (Measuring::debug != Measuring::MEASURING) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::ResponseState to GUI::IdleState"));
#endif // DEBUG
    StateMgr::switchTo(state, IdleState);
  }
  
  if (Promises::has_completed(button_l) || Promises::has_completed(button_r)) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::ResponseState to GUI::OverviewState"));
#endif // DEBUG
    StateMgr::switchTo(state, Menu::OverviewState);
  }
}
void GUI::MeasuringState_constr() {
  Actuators::LCD::printAt(F("Measuring data  "), 0);
  Actuators::LCD::printAt(F("Temp: working..."), 1);
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
}

void GUI::MeasuringState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
}

void GUI::MeasuringState_tick(State* state) {
  DisplayTemp(temp);

  if (Measuring::debug != Measuring::MEASURING) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::MeasuringState to GUI::IdleState"));
#endif // DEBUG
    StateMgr::switchTo(state, IdleState);
  }
  
  if (Promises::has_completed(button_l) || Promises::has_completed(button_r)) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::MeasuringState to GUI::OverviewState"));
#endif // DEBUG
    StateMgr::switchTo(state, Menu::OverviewState);
  }
}

void GUI::Menu::OverviewState_constr() {
  Actuators::LCD::printAt(F("Menu: Overview  "), 0);
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
  Promises::complete(button_l, (uint32_t)true);
  select = (byte)RETURN;
  
  if (Measuring::debug == Measuring::MEASURING) {
    // capture measuring data for pause
  }
}

void GUI::Menu::OverviewState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
}

void GUI::Menu::OverviewState_tick(State* state) {
  if (Promises::has_completed(button_l)) {
    select = ++select % OverviewMenu_length;
    switch (select) {
      case RETURN:
        Actuators::LCD::printAt(F("v Close Menu   >"), 1);
        break;
      case SPRAY_DELAY:
        Actuators::LCD::printAt(F("v Set Delay    >"), 1);
        break;
      case RESET_SPRAYS:
        Actuators::LCD::printAt(F("v Reset Sprays >"), 1);
        break;
      case ADVANCED:
        Actuators::LCD::printAt(F("v Advanced     >"), 1);
        break;
    }
    Promises::finalize(button_l);
    button_l = Sensors::Button::wait_for_next();
  }

  if (Promises::has_completed(button_r)) {
    switch (select) {
      case RETURN:
        if (Measuring::debug == Measuring::MEASURING) {
#ifdef DEBUG
          Serial.println(F("[DEBUG] Switch from GUI::Menu::OverviewState to GUI::MeasuringState"));
#endif // DEBUG
          StateMgr::switchTo(state, MeasuringState);
        } else {
#ifdef DEBUG
          Serial.println(F("[DEBUG] Switch from GUI::Menu::OverviewState to GUI::IdleState"));
#endif // DEBUG
          StateMgr::switchTo(state, IdleState);
        }
        break;
      case SPRAY_DELAY:
#ifdef DEBUG
        Serial.println(F("[DEBUG] Switch from GUI::Menu::OverviewState to GUI::Menu::SetDelayState"));
#endif // DEBUG
        StateMgr::switchTo(state, SetDelayState);
        break;
      case RESET_SPRAYS:
#ifdef DEBUG
        Serial.println(F("[DEBUG] Switch from GUI::Menu::OverviewState to GUI::Menu::ResetSprayState"));
#endif // DEBUG
        StateMgr::switchTo(state, ResetSprayState);
        break;
      case ADVANCED:
#ifdef DEBUG
        Serial.println(F("[DEBUG] Switch from GUI::Menu::OverviewState to GUI::Menu::AdvancedState"));
#endif // DEBUG
        StateMgr::switchTo(state, AdvancedState);
        break;
    }
  }
}

void GUI::Menu::ResetSprayState_constr() {
  Actuators::LCD::printAt(F("Confirm reset?  "), 0);
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
  Promises::complete(button_l, (uint32_t)true);
  select = (byte)false;
}

void GUI::Menu::ResetSprayState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
}

void GUI::Menu::ResetSprayState_tick(State* state) {
  if (Promises::has_completed(button_l)) {
    select = ++select % 2;
    switch (select) {
      case true:
        Actuators::LCD::printAt(F("v Confirm      >"), 1);
        break;
      case false:
        Actuators::LCD::printAt(F("v Cancel       >"), 1);
        break;
    }
    Promises::finalize(button_l);
    button_l = Sensors::Button::wait_for_next();
  }

  if (Promises::has_completed(button_r)) {
    switch (select) {
      case true:
        Storage::reset_sprays();
      case false:
#ifdef DEBUG
        Serial.println(F("[DEBUG] Switch from GUI::Menu::ResetSprayState to GUI::Menu::OverviewState"));
#endif // DEBUG
        StateMgr::switchTo(state, OverviewState);
        break;
    }
  }
}

void GUI::Menu::SetDelayState_constr() {
  Actuators::LCD::printAt(F("Set spray delay:"), 0);
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
  select = (byte)Storage::get_spray_warmup_time();
  Actuators::LCD::printAt(select, 1);
  Actuators::LCD::print(F("                "));
}

void GUI::Menu::SetDelayState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
}

void GUI::Menu::SetDelayState_tick(State* state) {
  if (Promises::has_completed(button_l)) {
    select = (select - 10) % 15 + 15;
    Actuators::LCD::printAt(select, 1);
    Actuators::LCD::print(F("                "));
    Promises::finalize(button_l);
    button_l = Sensors::Button::wait_for_next();
  }

  if (Promises::has_completed(button_r)) {
    Storage::set_spray_warmup_time(select);
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from GUI::Menu::SetDelayState to GUI::Menu::OverviewState"));
#endif // DEBUG
    StateMgr::switchTo(state, OverviewState);
  }
}

void GUI::Menu::AdvancedState_constr() {
  button_l = Sensors::Button::wait_for_next();
  button_r = Sensors::Button::wait_for_menu();
  Promises::complete(button_l, (uint32_t)true);
  select = (byte)RETURN_ADV;
}

void GUI::Menu::AdvancedState_destr() {
  Promises::finalize(button_l);
  Promises::finalize(button_r);
}

void GUI::Menu::AdvancedState_tick(State* state) {
  if (Promises::has_completed(button_l)) {
    select = ++select % AdvancedMenu_length;
    switch (select) {
      case RETURN_ADV:
        Actuators::LCD::printAt(F("Return to Menu  "), 0);
        Actuators::LCD::printAt(F("                "), 1);
        break;
      case PROMISES:
        Actuators::LCD::printAt(F("Active Promises "), 0);
        break;
      case LIGHTSENSOR:
        Actuators::LCD::printAt(F("Light sensor    "), 0);
        break;
      case WRITECACHE:
        Actuators::LCD::printAt(F("Disk-Write Cache"), 0);
        Actuators::LCD::printAt(F("  Confirm     ->"), 1);
        break;
#ifdef DEBUG
      case CACHETOSERIAL:
        Actuators::LCD::printAt(F("Cache to serial?"), 0);
        Actuators::LCD::printAt(F("  Confirm     ->"), 1);
        break;
#endif
      case RESET_DEFAULTS:
        Actuators::LCD::printAt(F("Reset defaults? "), 0);
        Actuators::LCD::printAt(F("  Confirm     ->"), 1);
        break;
      default:
        break;
    }
    Promises::finalize(button_l);
    button_l = Sensors::Button::wait_for_next();
  }

  switch (select) {
    case PROMISES:
      Actuators::LCD::printAt(Promises::count(), 1);
      Actuators::LCD::print(F("                "));
      break;
    case LIGHTSENSOR:
      Actuators::LCD::printAt(Sensors::Light::last_measurement, 1);
      Actuators::LCD::print(F("                "));
      break;
    default:
      break;
  }

  if (Promises::has_completed(button_r)) {
    switch (select) {
      case RETURN_ADV:
#ifdef DEBUG
        Serial.println(F("[DEBUG] Switch from GUI::Menu::AdvancedState to GUI::Menu::OverviewState"));
#endif // DEBUG
        StateMgr::switchTo(state, OverviewState);
        break;
      case WRITECACHE:
        Storage::write();
        Actuators::LCD::printAt(F("  Done!         "), 1);
        break;
#ifdef DEBUG
      case CACHETOSERIAL:
        Storage::serial_export();
        Actuators::LCD::printAt(F("  Done!         "), 1);
        break;
#endif // DEBUG
      case RESET_DEFAULTS:
        Storage::reset_defaults();
        Actuators::LCD::printAt(F("  Done!         "), 1);
        break;
      default:
        break;
    }
    Promises::finalize(button_r);
    button_r = Sensors::Button::wait_for_menu();
  }
}

#endif // GUI_H
