#ifndef ACTUATORS_H
#define ACTUATORS_H

#include "base.h"
#include <LiquidCrystal.h>
#include "storage.h"

// define pins, for faster rearangement when neccessary

// digital pins
// 0 and 1 are not available when using serial communication
const byte LCD_D7      = 2;
const byte LCD_D6      = 3;
const byte LCD_D5      = 4;
const byte LCD_D4      = 5;
// 6,7,8 are sensor pins
const byte LED_RED     = 9;
const byte LED_GREEN   = 10;
const byte LED_BLUE    = 11;
// 12 is a sensor pin
const byte SPRAY_RELAY = 13; // same as internal LED, useful for testing purposes

// analog pins
const byte LCD_RS      = A0;
const byte LCD_EN      = A1;
// A2 - A5 are sensor pins

namespace Actuators {
  void init();
  void tick();

  namespace LCD {
    namespace { // anonymous namespace (privates)
      LiquidCrystal screen(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
    }
    
    void init();
    void print(const char* text);
    void print(const __FlashStringHelper* text);
    void print(float text);
    void print(int text);
    void printAt(const char* text,                byte row);
    void printAt(const __FlashStringHelper* text, byte row);
    void printAt(float text,                      byte row);
    void printAt(int text,                        byte row);
  }
  
  namespace Freshener {
    namespace {
      unsigned long last_sprayed;
      long timer;
      bool executing_spray;
      bool is_available;
      // adress of sprays_left variable in EEPROM memory
      int sprays_left_adress;
      // amount of sprays in one can
      const unsigned int can_contents = 1000;
      // time (in seconds) till spray actually happens (hardware limitation)
      const int warmup = 15;
      const int cooldown = 2;
    }
    void init();
    void tick();

    // wrapped values for safety
    bool available(); // if true, execute_spray can be called
    unsigned long elapsed_since_last_spray(); // in seconds
        
    void execute_spray();

    int sprays_left();    
    void reset_sprays_left();
    void decrease_sprays_left();
    void set_sprays_left(unsigned int value);
  }

  namespace MultiColorLED {
    // init pins
    void init();
    // specify color
    void set_color(byte R, byte G, byte B);
  }
}

// .ccp's

void Actuators::init(){
  LCD::init();
  Freshener::init();
  MultiColorLED::init();
}

void Actuators::tick(){
  Freshener::tick();
}

// LCD
void Actuators::LCD::init() {
  screen.begin(16, 2);
}

void Actuators::LCD::print(int text){
  screen.print(text);
}

void Actuators::LCD::print(float text){
  screen.print(text);
}

void Actuators::LCD::print(const char* text){
  screen.print(text);
}

void Actuators::LCD::print(const __FlashStringHelper* text) {
  screen.print(text);
}

void Actuators::LCD::printAt(int text, byte row){
  screen.setCursor(0, row);
  Actuators::LCD::print(text);
}

void Actuators::LCD::printAt(float text, byte row){
  screen.setCursor(0, row);
  Actuators::LCD::print(text);
}

void Actuators::LCD::printAt(const char* text, byte row){
  screen.setCursor(0, row);
  Actuators::LCD::print(text);
}

void Actuators::LCD::printAt(const __FlashStringHelper* text, byte row) {
  screen.setCursor(0, row);
  Actuators::LCD::print(text);
}

// Freshener
void Actuators::Freshener::init(){
  pinMode(SPRAY_RELAY, OUTPUT);
}

void Actuators::Freshener::tick(){
  if (executing_spray && Timer::elapsed_seconds(timer) > warmup){
    decrease_sprays_left();
    executing_spray = false;
    digitalWrite(SPRAY_RELAY, LOW);
    last_sprayed = Timer::start();
#ifdef DEBUG
    Serial.println(F("[DEBUG] End spray actuator"));
#endif // DEBUG
  }
  is_available = !executing_spray && Timer::elapsed_seconds(timer) > warmup + cooldown;
}

bool Actuators::Freshener::available(){
  return is_available;
}

unsigned long Actuators::Freshener::elapsed_since_last_spray(){
  return Timer::elapsed_seconds(last_sprayed);
}

void Actuators::Freshener::execute_spray(){
  timer = Timer::start();
  digitalWrite(SPRAY_RELAY, HIGH);
  executing_spray = true;
#ifdef DEBUG
  Serial.println(F("[DEBUG] Begin spray actuator"));
#endif // DEBUG
}

int Actuators::Freshener::sprays_left(){
  return Storage::get_sprays_remaining();
}

void Actuators::Freshener::reset_sprays_left(){
  set_sprays_left(can_contents);
}

void Actuators::Freshener::decrease_sprays_left(){
  int current = sprays_left();
  // prevent overflowing spray count
  if (current > 0){
     set_sprays_left(current - 1);
  }
}

void Actuators::Freshener::set_sprays_left(unsigned int value){
  Storage::set_sprays_remaining(value);
}

// MultiColorLED
void Actuators::MultiColorLED::init()
{
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
}

void Actuators::MultiColorLED::set_color(byte red, byte green, byte blue){
  analogWrite(LED_RED, red);
  analogWrite(LED_GREEN, green);
  analogWrite(LED_BLUE, blue);
}

#endif // Actuators_H
