#ifndef MEASURING_H
#define MEASURING_H

#include "base.h"
#include "storage.h"
#include "sensors.h"
#include "freshener.h"

// .h's
namespace Measuring {
  namespace {
    // Globals
    Promise* current_waiting = 0;
    bool door_conn;
    bool seat_conn;

    Promise* p_movement;
    unsigned long movement;
    unsigned long start;
  }
  long  get_movement() { return movement; }
  long get_start()    { return start; }
  
  // State Fumctions
  void InitializationState_constr();
  void InitializationState_destr();
  void InitializationState_tick(State* current);
  
  void WaitingState_constr();
  void WaitingState_destr();
  void WaitingState_tick(State* current);
  
  void MeasuringState_constr();
  void MeasuringState_destr();
  void MeasuringState_tick(State* current);
  
  typedef enum debug_t { INIT, WAITING, MEASURING } debug_t;
  debug_t debug = INIT;

  typedef enum usage { CLEANUP, NUMBER1, NUMBER2 } usage;
  typedef struct Input {
    bool input_given;
    usage input_usage;
  } Input;
  Input session = { 0 };
  
  // States
  State InitializationState {
    InitializationState_constr,
    InitializationState_destr,
    InitializationState_tick
  };
  
  State WaitingState {
    WaitingState_constr,
    WaitingState_destr,
    WaitingState_tick
  };
  
  State MeasuringState {
    MeasuringState_constr,
    MeasuringState_destr,
    MeasuringState_tick
  };
}

// .cpp's
void Measuring::InitializationState_constr() {
  debug = INIT;
  // Test if door (magn) connected
  // TODO
  
  // Wait until the room is clear based on most simple data
  current_waiting = Sensors::Light::wait_for_off();
}

void Measuring::InitializationState_destr() {
  Promises::finalize(current_waiting);
}

void Measuring::InitializationState_tick(State* current) {
  // When room is clear, start the program
  if (Promises::has_completed(current_waiting) && Actuators::Freshener::available()) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from Initialization to WaitingState"));
#endif
    StateMgr::switchTo(current, WaitingState);
  }
}

Promise* wait_for_person() {
  //return Sensors::Motion::wait_for();
  return Sensors::Light::wait_for_on();
}

Promise* wait_for_clear() {
  // Return promise based upon initalization data
  return Sensors::Light::wait_for_off();
}

void Measuring::WaitingState_constr() {
  debug = WAITING;
  current_waiting = wait_for_person();
}

void Measuring::WaitingState_destr() {
  Promises::finalize(current_waiting);
}

void Measuring::WaitingState_tick(State* current) {
  if (Promises::has_completed(current_waiting)) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from WaitingState to MeasuringState"));
#endif
    StateMgr::switchTo(current, MeasuringState);
  }
}

void Measuring::MeasuringState_constr() {
  debug = MEASURING;
  session = { 0 };
  start = Timer::start();
  movement = 0;
  p_movement = Sensors::Motion::wait_for();
  current_waiting = wait_for_clear();
}

float CalcConf(Storage::Avg avg, float value) {
  return max(0.0f, 1.0f - abs((float)value - avg.mean) / avg.sd * 2);
}

void Measuring::MeasuringState_destr() {
  // Gather data
  long elapsed = Timer::elapsed_seconds(start);

  // Analyse data
  usage action;
  if (session.input_given) {
    // store data to disk & sync
    switch (session.input_usage) {
      case NUMBER1: // if case 1
        Storage::update_number1(elapsed, movement);
        break;
      case NUMBER2: // if case 2
        Storage::update_number2(elapsed, movement);
        break;
      default: // if cleanup
        Storage::update_cleanup(elapsed, movement);
        break;
    }

    // Set action
    action = session.input_usage;
  } else {
    // Calculate likelyhood of each action
    // Number 1
    float n1_time   = CalcConf(Storage::get_number1_time(),   (float)elapsed);
    float n1_motion = CalcConf(Storage::get_number1_motion(), (float)movement);

    float n1_time_sd   = Storage::get_number1_time().sd    / Storage::get_number1_time().mean;
    float n1_motion_sd = Storage::get_number1_motion().sd  / Storage::get_number1_motion().mean;
    float n1_sd = n1_time_sd + n1_motion_sd;

    float n1_time_imp   = 1.0f - n1_time_sd   / n1_sd; // small sd -> high importance
    float n1_motion_imp = 1.0f - n1_motion_sd / n1_sd;

    float number1_likelyhood = n1_time * n1_time_imp + n1_motion * n1_motion_sd;

    // Number 2
    float n2_time   = CalcConf(Storage::get_number2_time(),   (float)elapsed);
    float n2_motion = CalcConf(Storage::get_number2_motion(), (float)movement);
    
    float n2_time_sd   = Storage::get_number2_time().sd    / Storage::get_number2_time().mean;
    float n2_motion_sd = Storage::get_number2_motion().sd  / Storage::get_number2_motion().mean;
    float n2_sd = n2_time_sd + n2_motion_sd;

    float n2_time_imp   = 1.0f - n2_time_sd   / n2_sd;
    float n2_motion_imp = 1.0f - n2_motion_sd / n2_sd;

    float number2_likelyhood = n2_time * n2_time_imp + n2_motion * n2_motion_sd;
    
    // Cleanup
    float c_time   = CalcConf(Storage::get_cleanup_time(),   (float)elapsed);
    float c_motion = CalcConf(Storage::get_cleanup_motion(), (float)movement);
    
    float c_time_sd   = Storage::get_cleanup_time().sd    / Storage::get_cleanup_time().mean;
    float c_motion_sd = Storage::get_cleanup_motion().sd  / Storage::get_cleanup_motion().mean;
    float c_sd = c_time_sd + c_motion_sd;

    float c_time_imp   = 1.0f - c_time_sd   / c_sd;
    float c_motion_imp = 1.0f - c_motion_sd / c_sd;

    float cleanup_likelyhood = c_time * c_time_imp + c_motion * c_motion_sd;
    
    // Choose action
    float action_value = Storage::get_confidence_threshold();
    action = CLEANUP;

    if (number1_likelyhood >= action_value) {
      action_value = number1_likelyhood;
      action = NUMBER1;
    }
    
    if (number2_likelyhood >= action_value) {
      action_value = number2_likelyhood;
      action = NUMBER2;
    }

    if (cleanup_likelyhood >= action_value) {
      action_value = cleanup_likelyhood;
      action = CLEANUP;
    }

#ifdef DEBUG
    Serial.print(F("[DEBUG] Elapsed time: "));
    Serial.println(elapsed);
    Serial.print(F("[DEBUG] Movement: "));
    Serial.println(movement);
    Serial.print(F("[DEBUG] Number1 time val: "));
    Serial.println(n1_time);
    Serial.print(F("[DEBUG] Number1 motion val: "));
    Serial.println(n1_motion);
    Serial.print(F("[DEBUG] Number1 likelyhood: "));
    Serial.println(number1_likelyhood);
    Serial.print(F("[DEBUG] Number2 time val: "));
    Serial.println(n2_time);
    Serial.print(F("[DEBUG] Number2 motion val: "));
    Serial.println(n2_motion);
    Serial.print(F("[DEBUG] Number2 likelyhood: "));
    Serial.println(number2_likelyhood);
    Serial.print(F("[DEBUG] Cleanup time val: "));
    Serial.println(c_time);
    Serial.print(F("[DEBUG] Cleanup motion val: "));
    Serial.println(c_motion);
    Serial.print(F("[DEBUG] Cleanup likelyhood: "));
    Serial.println(cleanup_likelyhood);
    
    switch (action) {
      case NUMBER1:
        Serial.print(F("[DEBUG] Action1 chosen with confidence: "));
        Serial.println(action_value);
        break;
      case NUMBER2:
        Serial.print(F("[DEBUG] Action2 chosen with confidence: "));
        Serial.println(action_value);
        break;
      default:
        Serial.print(F("[DEBUG] No action chosen with confidence: "));
        Serial.println(action_value);
        break;
    }
#endif // DEBUG
  }

  // Execute action
  switch (action) {
    case NUMBER1: // if case 1
      Freshener::action1();
      break;
    case NUMBER2: // if case 2
      Freshener::action2();
      break;
    default:
      break;
  }

  // Cleanup
  Promises::finalize(p_movement);
  Promises::finalize(current_waiting);
}

void Measuring::MeasuringState_tick(State* current) {
  // Gather data
  if (Promises::has_completed(p_movement)) {
    movement++;
    Promises::finalize(p_movement);
    p_movement = Sensors::Motion::wait_for();
  }
  
  if (Promises::has_completed(current_waiting)) {
#ifdef DEBUG
    Serial.println(F("[DEBUG] Switch from MeasuringState to WaitingState"));
#endif // DEBUG
    StateMgr::switchTo(current, WaitingState);
  }
}

#endif // MEASURING_H
