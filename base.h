  #ifndef BASE_H
#define BASE_H

#define DEBUG

#ifndef PROMISE_MAX_AMOUNT
#define PROMISE_MAX_AMOUNT 16
#endif // PROMISE_MAX_AMOUNT

// .h's
// States
typedef struct State {
  void (* constr)();
  void (*  destr)();
  void (*   tick)(State* current);
} State;

namespace StateMgr {
  void default_constr() { };
  void default_destr() { };
  
  void initMgr(State state);
  void dropMgr(State* state);
  void tick(State* state);
  void switchTo(State* state, State new_state);
}

// Promises
typedef struct Promise {
  void (*check)(Promise* self);
  volatile uint32_t result;
} Promise;

namespace Promises {
  namespace {
    Promise heap[PROMISE_MAX_AMOUNT]; // fixed size, reboot when overflowing
    bool     any[PROMISE_MAX_AMOUNT] = { 0 };
    void null_fn(Promise* self) { }
    
    Promise* INT0_promise = 0;
    volatile void (*INT0_fn)(Promise* self);

    Promise* INT1_promise = 0;
    volatile void (*INT1_fn)(Promise* self);
  }

  void INT0_wrapper();
  void INT1_wrapper();

  Promise* create(void (*check)(Promise* self)); // for tick based
  Promise* create(int interrupt, void (*exec)(Promise* self)); // for interrupt based

  bool has_completed(Promise* promise);
  void complete(Promise* promise, uint32_t result);
  void finalize(Promise* promise);
  void exec_checks();
  int  count();
}

// timing
namespace Timer {
  // set starting time 
  unsigned long start();
  // handles rollover, time in milliseconds
  unsigned long elapsed(unsigned long timer);
  // handles rollover, time in seconds
  unsigned long elapsed_seconds(unsigned long timer);
}

// .cpp's
// States
void StateMgr::initMgr(State state) {
  state.constr();
}

void StateMgr::dropMgr(State* state) {
  state->destr();
}

void StateMgr::tick(State* state) {
  state->tick(state);
}

void StateMgr::switchTo(State* state, State new_state) {
  dropMgr(state);
  initMgr(new_state);
  *state = new_state;
}

void Promises::INT0_wrapper() {
  if (Promises::INT0_fn != 0) {
    Promises::INT0_fn(Promises::INT0_promise);
    Promises::INT0_fn = 0;
  }
}

void Promises::INT1_wrapper() {
  if (Promises::INT1_fn != 0) {
    Promises::INT1_fn(Promises::INT1_promise);
    Promises::INT1_fn = 0;
  }
}

// Promises
Promise* Promises::create(void (*check)(Promise* self)) {
  for (int i = 0; i < PROMISE_MAX_AMOUNT; ++i) if (!Promises::any[i]) {
    Promises::heap[i] = Promise { check, 0 };
    Promises::any[i]  = true;
    return Promises::heap + i;
  }
#ifdef DEBUG
  Serial.println("[ERROR] Memory overflown, system may behave unexpectedly");
#endif // DEBUG
  return 0;
}

Promise* Promises::create(int interrupt, void (*exec)(Promise* self)) {
  if (interrupt != 2 || interrupt != 3)
    return 0;
  
  for (int i = 0; i < PROMISE_MAX_AMOUNT; ++i) if (!Promises::any[i]) {
    Promises::heap[i] = Promise { Promises::null_fn, 0 };
    Promises::any[i]  = true;
    Promise* ptr = Promises::heap + i;
    switch (interrupt) {
      case 2: // INT0
        Promises::INT0_promise = ptr;
        Promises::INT0_fn = exec;
        attachInterrupt(digitalPinToInterrupt(2), INT0_wrapper, CHANGE);
        break;
      case 3: // INT1
        Promises::INT1_promise = ptr;
        Promises::INT1_fn = exec;
        attachInterrupt(digitalPinToInterrupt(3), INT1_wrapper, CHANGE);
        break;
    }
    return ptr;
  }
#ifdef DEBUG
  Serial.println("[ERROR] Memory overflown, system may behave unexpectedly");
#endif // DEBUG
  return 0;
}

bool Promises::has_completed(Promise* promise) {
  return promise->result != 0;
}

void Promises::complete(Promise* promise, uint32_t result) {
  if (!Promises::has_completed(promise))
    promise->result = result;
}

void Promises::finalize(Promise* promise) {
  Promises::complete(promise, 1);
  if (INT0_promise == promise) {
    detachInterrupt(digitalPinToInterrupt(2));
    INT0_promise = 0;
    INT0_fn = 0;
  }
  if (INT1_promise == promise) {
    detachInterrupt(digitalPinToInterrupt(3));
    INT1_promise = 0;
    INT1_fn = 0;
  }
  if (promise >= Promises::heap && promise < Promises::heap + PROMISE_MAX_AMOUNT) {
    int index = promise - Promises::heap;
    Promises::any[index] = false;
  }
}

void Promises::exec_checks() {
  for (int i = 0; i < PROMISE_MAX_AMOUNT; ++i) if (Promises::any[i]) {
    Promise* current = Promises::heap + i;
    if (!Promises::has_completed(current))
      current->check(current);
  }
}

int Promises::count() {
  int ctr = 0;
  for (int i = 0; i < PROMISE_MAX_AMOUNT; ++i) if (Promises::any[i]) {
    ctr++;
  }
  return ctr;
}

// Timing
unsigned long Timer::start(){
  return millis();
}

unsigned long Timer::elapsed(unsigned long timer){
  return millis() - timer;
}

unsigned long Timer::elapsed_seconds(unsigned long timer){
  return (millis() - timer) / 1000;
}

#endif // BASE_H
